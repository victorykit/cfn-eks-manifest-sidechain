import distutils.cmd
import distutils.log
import setuptools
import subprocess
from pathlib import Path
from configparser import ConfigParser

class DockerBuildCommand(distutils.cmd.Command):
  """Docker Binary Distribution Build"""

  description = 'build AWS Lambda Layer with Docker'
  user_options = [
      ('directory=', 'd', 'distribution directory')
  ]

  def initialize_options(self):
    """Set default values for options."""

    setup_cfg = ConfigParser()
    setup_cfg.optionxform = str
    setup_cfg.read(Path(__file__).parent / 'setup.cfg')

    self.directory = str(Path(__file__).parent / 'dist')

    self.name = setup_cfg['metadata']['name']
    self.version = setup_cfg['metadata']['version']
    self.dist_basename = f'{self.name}-{self.version}.zip'

    self.tag_prefix = 'local/'
    self.build_dir = '/tmp'
    self.dist_dir = '/'
    if 'docker' in setup_cfg:

        if 'tag_prefix' in setup_cfg['docker'].keys():
            self.tag_prefix = setup_cfg['docker']['tag_prefix']
        if 'build_dir' in setup_cfg['docker'].keys():
            self.build_dir = setup_cfg['docker']['build_dir']
        if 'dist_dir' in setup_cfg['docker'].keys():
            self.dist_dir = setup_cfg['docker']['dist_dir']


    self.tag_name = f'{self.tag_prefix}{self.name}:{self.version}'


    self.build_args = {}
    if 'docker.build_args' in setup_cfg:
        self.build_args = setup_cfg['docker.build_args']

  def finalize_options(self):
    """Post-process options."""

    pass

  def run(self):
    """Run command."""

    build_cmd = [
        'docker',
        'build',
        '-t', self.tag_name,
        '--build-arg', f'DIST_BASENAME={self.dist_basename}',
        '--build-arg', f'BUILD_DIR={self.build_dir}',
        '--build-arg', f'DIST_DIR={self.dist_dir}']

    for argk, argv in self.build_args.items():

        build_cmd.append('--build-arg')
        build_cmd.append(f'{argk}={argv}')

    build_cmd.append('.')

    self.announce(
        'building docker image: %s' %(' '.join(build_cmd),),
        level=distutils.log.INFO)
    subprocess.check_call(build_cmd, cwd=Path(__file__).parent)

    run_cmd = ['docker', 'run', '-d', self.tag_name ,'false']
    self.announce(
        'initializing temporary docker container: %s' %(' '.join(run_cmd),),
        level=distutils.log.INFO)
    self.container_id = subprocess.check_output(run_cmd, cwd=Path(__file__).parent).strip().decode('utf-8')

    assert self.container_id, 'No container id returned.'

    Path(self.directory).mkdir(exist_ok=True)

    local_build_dir = Path(__file__).parent / 'build'
    local_build_dir.mkdir(exist_ok=True)

    build_cp_cmd = [
        'docker', 
        'cp',
        f'{self.container_id}:{self.build_dir}/.',
        str(local_build_dir)]

    self.announce(
        'extracting build from docker image: %s' %(' '.join(build_cp_cmd),),
        level=distutils.log.INFO)

    subprocess.check_call(build_cp_cmd, cwd=Path(__file__).parent)

    dist_cp_cmd = [
        'docker', 
        'cp',
        f'{self.container_id}:/{self.dist_basename}',
        f'{self.directory}/{self.dist_basename}']

    self.announce(
        'extracting dist from docker image: %s' %(' '.join(dist_cp_cmd),),
        level=distutils.log.INFO)

    subprocess.check_call(dist_cp_cmd, cwd=Path(__file__).parent)


setuptools.setup(
    cmdclass={
        'bdist': DockerBuildCommand,
    }
)