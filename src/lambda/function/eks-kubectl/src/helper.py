#!/usr/bin/env python3
import subprocess, os, tempfile
import boto3, botocore.exceptions


TEMPDIR = tempfile.gettempdir()
KUBECONFIGDIRPATH = os.path.join( TEMPDIR, '.kube' )
KUBECONFIGBASENAME = 'config'
KUBECONFIGPATH = os.path.join( KUBECONFIGDIRPATH, KUBECONFIGBASENAME )
KUBEMANIFESTSDIRPATH = os.path.join( TEMPDIR, 'manifests' )
KUBEMANIFESTSBASENAMEPREFIX = 'manifest'



class ShellException( BaseException ):
    """Exception that occurs during a shell command operation
    """

    pass



def runShellCommand( cmd ):
    """execute a shell command

    :param cmd: shell command fields
    :type cmd: tuple

    :return: returns shell's stdout, and command exit code as a tuple
    :rtype: tuple
    """

    assert isinstance( cmd, tuple )

    process = subprocess.Popen( cmd, stdout=subprocess.PIPE )

    return process.communicate()[ 0 ].decode( 'utf-8' ), process.returncode, ' '.join( cmd )



def getKubectlManifestCommand( action, **kwargs ):
    """get a kubectl shell command for working with a manifest

    :param action: kubectl action to perform
    :type action: str

    **manifestPath (str): path to a Kubernetes manifest file

    :return: returns shell command fields
    :rtype: tuple
    """

    actionFields = {
        'apply':  [ '-f', kwargs[ 'manifestPath' ] ],
        'delete': [ '-f', kwargs[ 'manifestPath' ] ]
    }[ action ]

    return tuple( [
        *[ 'kubectl', action ],
        *actionFields
    ] )



def getEksClusterStatus( clusterName, client = None ):
    """get the status of an AWS EKS cluster

    :param clusterName: name of an existing AWS EKS cluster
    :type action: str

    :param clusterName: a boto3 `eks` client
    :type action: boto3.EKS.Client, optional

    :return: returns cluster status
    :rtype: str
    """

    client = boto3.client( 'eks' ) if client == None else client

    try:

        response = client.describe_cluster(
            name = clusterName
        )
    except botocore.exceptions.ClientError:

        return None

    return response[ 'cluster' ][ 'status' ]



def getTerminalEksClusterStatus( clusterName, client = None, wait = 10 ):
    """get an EKS cluster status, which is terminal

    :param clusterName: name of an existing AWS EKS cluster
    :type action: str

    :param clusterName: a boto3 `eks` client
    :type action: boto3.EKS.Client, optional

    :return: returns a terminal cluster status
    :rtype: str
    """

    import time

    nonTerminalStatus = [ 'CREATING', 'UPDATING' ]

    while getEksClusterStatus( clusterName, client ) in nonTerminalStatus:

        time.sleep( wait )

    return getEksClusterStatus( clusterName, client )



def dedent( text ):

    wprefix = ''

    for char in text:

        if char not in [ ' ', '\t' ]: break

        wprefix += char

    lines = []

    for line in text.split( '\\n' ):

        lines.append( line[len(wprefix):] )

    return '\n'.join( lines )



def writeDataToFile( *data, **kwargs ):
    """write a set of arbitrary data to file(s)

    *data (str): non-binary data
    **baseDirPath (str): base directory path to write files to 
    **prefix (str): file basename prefix

    :return: returns a list of paths of written files
    :rtype: tuple
    """

    baseDirPath = kwargs[ 'baseDirPath' ] if 'baseDirPath' in kwargs.keys() \
        else KUBEMANIFESTSDIRPATH

    prefix = kwargs[ 'prefix' ] if 'prefix' in kwargs.keys() \
        else KUBEMANIFESTSBASENAMEPREFIX

    paths = []

    os.makedirs( baseDirPath, exist_ok = True )

    for i in range( len( data ) ):

        _data = dedent( data[ i ].replace( '\\\\', '\\' ) )

        _path = os.path.join( baseDirPath, '%s%d.yml' % ( prefix, i ) )

        with open( _path, 'w' ) as fh: fh.write( _data )

        paths.append( _path )

    return tuple( paths )



def getKubeconfig( clusterName, **kwargs ):
    """get a kubectl configuration via the AWS CLI

    :param clusterName: name of an existing AWS EKS cluster
    :type action: str

    :return: returns path to kube config file
    :rtype: str
    """

    configPath = kwargs[ 'configPath' ] if 'configPath' in kwargs.keys() \
        else KUBECONFIGPATH

    os.makedirs( os.path.dirname( configPath ), exist_ok = True )

    os.environ[ 'KUBECONFIG' ] = configPath

    shellCommand = tuple( [
        "aws", "eks",
            "update-kubeconfig", "--name", clusterName
    ] )

    output, exitCode, cmd = runShellCommand( shellCommand )

    del os.environ[ 'KUBECONFIG' ]

    if exitCode != 0: raise ShellException( output )

    return configPath



def executeManifestData( action, *manifest, **kwargs ):
    """execute manifests via kubectl

    :param action: kubectl CLI action field
    :type action: str

    *manifests (str): non-binary manifest data

    :return: returns shell output
    :rtype: tuple
    """

    configPath = kwargs[ 'configPath' ] if 'configPath' in kwargs.keys() \
        else KUBECONFIGPATH

    os.environ[ 'KUBECONFIG' ] = configPath

    paths = writeDataToFile( *manifest )

    out = []

    for path in paths:

        shellCommand = getKubectlManifestCommand( action, manifestPath = path )

        out.append( runShellCommand( shellCommand ) )

    del os.environ[ 'KUBECONFIG' ]

    return tuple( out )