#!/usr/bin/env python3
"""Execute Kubernetes manifests on an AWS EKS cluster

This module can be used as a standalone AWS Lambda function, or AWS 
Lambda-backed CloudFormation custom resource.

This module requires an AWS Lambda layer, which provides the awscli and kubectl
executables.

To use as a standalone AWS lambda function, use the `lambdaHandler` method.

To use as an AWS Lambda-backed CloudFormation custom resource handler, use the 
`cfnHandler` method as a `ServiceToken` backend.
"""
import os, json
import boto3

import cfnresponse
from helper import (
    getTerminalEksClusterStatus,
    getKubeConfig,
    executeManifestData,
    ShellException
)


class LambdaHandlerException( BaseException ):
    """Exception that occurs during a shell command operation
    """

    pass



def lambda_handler( event, context ):
    """generic handler for AWS Lambda function
    """

    assert 'manifest' in event.keys(),  "'manifest' not in 'event' keys."
    assert 'action' in event.keys(),  "'action' not in 'event' keys."

    if 'CLUSTER_NAME' in dict( os.environ ).keys():
        clusterName = os.environ[ 'CLUSTER_NAME' ]
    elif 'clusterName' in event.keys() and event[ 'clusterName' ] != None:
        clusterName = event[ 'clusterName' ]
    else:
        raise KeyError( "'clusterName' not in 'event' keys." )

    clusterStatus = getTerminalEksClusterStatus( 
        clusterName,
        boto3.client( 'eks' )
    )

    if clusterStatus == None:

        return {
            'statusCode': 404,
            'headers': { 'Content-Type': 'text/plain' },
            'body': f"EKS Cluster '{clusterName}' does not exist."
        }
    elif clusterStatus in [ 'FAILED', 'DELETING' ]:

        return {
            'statusCode': 403,
            'headers': { 'Content-Type': 'text/plain' },
            'body': f"Cluster status is '{status}'."
        }

    try:

        getKubeconfig( clusterName )
    except BaseException as e:

        return {
            'statusCode': 500,
            'headers': { 'Content-Type': 'text/plain' },
            'body': f"Unable to get kubeconfig: %s" % ( str( e ) )
        }

    executionResults = None
    try:

        if isinstance( event[ 'manifest' ], str ):

            event[ 'manifest' ] = [ event[ 'manifest' ] ]

        executionResults = executeManifestData( 
            event[ 'action' ],
            *event[ 'manifest' ]
        )

        for result in executionResults:

            msg = "Command '%s' returned non-zero status code [%d]: %s " % (
                result[ 2 ], result[ 1 ], result[ 0 ]
            )

            if result[ 1 ] != 0: raise ShellException( msg )

    except BaseException as e:

        return {
            'statusCode': 500,
            'headers': { 'Content-Type': 'text/plain' },
            'body': f"Unable to execute manifest(s): %s" %( str( e ) )
        }

    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps( executionResults )
    }



def cfn_handler( event, context ):
    """handler for Lambda-backed CloudFormation custom resources
    """

    _event = {
        'manifest': None,
        'action': None,
        'clusterName': None
    }

    #cloudformation handler initialization
    try:
        if 'Manifest' not in event[ 'ResourceProperties' ].keys():

            raise KeyError( "Resource Property 'Manifest' not specified." )

        if isinstance( event[ 'ResourceProperties' ][ 'Manifest' ], str ) == False and \
           isinstance( event[ 'ResourceProperties' ][ 'Manifest' ], list ) == False:

            msg  = "Resource property 'Manifest' must be a string or list of "
            msg += "strings."

            raise TypeError( msg )

        _event[ 'manifest' ] = event[ 'ResourceProperties' ][ 'Manifest' ]

        if 'ClusterName' in event[ 'ResourceProperties' ].keys():

            _event[ 'clusterName' ] = event[ 'ResourceProperties' ][ 'ClusterName' ]
        elif 'CLUSTER_NAME' not in dict( os.environ ).keys():

            msg  = "Neither resource property 'ClusterName', nor environment "
            msg += "variable 'CLUSTER_NAME' specified."

            raise AttributeError( msg )

        if event[ 'RequestType' ] == 'Delete': _event[ 'action' ] = 'delete'
        else: _event[ 'action' ] = 'apply'
    except BaseException as e:

        responseData = {
            'Reason': 'Error during handler initialization: %s' % ( str( e ) )
        }

        cfnresponse.send( event, context, cfnresponse.FAILED, responseData, '' )

    #lambda handler execution
    try:

        result = lambdaHandler( _event, context )

        if result[ 'statusCode' ] != 200:

            raise LambdaHandlerException( result[ 'body' ] )

        trueManifestCount = len( json.loads( result[ 'body' ] ) )

        responseData = {
            'Reason': '%d manifests executed: %s' % ( trueManifestCount, result[ 'body' ] )
        }

        cfnresponse.send( event, context, cfnresponse.SUCCESS, responseData, '' )

    except BaseException as e:

        responseData = {
            'Reason': '%s: %s' % ( e.__class__.__name__, str( e ) )
        }

        cfnresponse.send( event, context, cfnresponse.FAILED, responseData, '' )
