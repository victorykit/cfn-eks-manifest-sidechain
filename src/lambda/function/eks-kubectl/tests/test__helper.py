#!/usr/bin/env python3
"""
"""
import pytest, os

@pytest.fixture
def module():

    import _helper as module

    return module


@pytest.fixture
def mock():

    import unittest.mock

    return unittest.mock


@pytest.fixture
def maintainKubeManifestDir():

    import _helper as module

    cleanup = False
    if os.path.exists( module.KUBEMANIFESTSDIRPATH ) == False: cleanup = True

    yield

    if cleanup == True: os.rmdir( module.KUBEMANIFESTSDIRPATH )



class Test_getKubectlManifestCommand():


    def test_apply( self, module ):

        result = module.getKubectlManifestCommand( 
            'apply',
            manifestPath='foo/bar'
        )

        assert result == tuple( [ 'kubectl', 'apply', '-f', 'foo/bar' ] )


    def test_delete( self, module ):

        result = module.getKubectlManifestCommand( 
            'delete',
            manifestPath='foo/bar'
        )

        assert result == tuple( [ 'kubectl', 'delete', '-f', 'foo/bar' ] )


    def test_invalidApply( self, module ):

        with pytest.raises( KeyError ):
            module.getKubectlManifestCommand( 
                'apply'
            )


    def test_invalidDelete( self, module ):

        with pytest.raises( KeyError ):
            module.getKubectlManifestCommand( 
                'delete'
            )


    def test_invalidAction( self, module ):

        with pytest.raises( KeyError ):
            module.getKubectlManifestCommand( 
                'foobar'
            )



class Test_getEksClusterStatus():


    def _mockTerminalResponse( self, operationName, kwargs ):

        if operationName == 'DescribeCluster':
            return {
                'cluster': {
                    'name': kwargs[ 'name' ],
                    'status': 'ACTIVE'
                }
            }
        else:
            raise BaseException( 'Wrong command: %s' % ( operationName ) )


    def test_existingClusterWithDefaultClient( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockTerminalResponse
        ):

            result = module.getEksClusterStatus( 'foo' )

            assert result == 'ACTIVE'


    def test_existingClusterWithCustomClient( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockTerminalResponse
        ):

            import boto3

            client = boto3.client( 'eks' )

            result = module.getEksClusterStatus( 'foo', client )

            assert result == 'ACTIVE'



class Test_getTerminalEksClusterStatus():

    _nonTerminalRoundTripIndex = 0

    def _mockNonTerminalResponse( self, operationName, kwargs ):

        if operationName == 'DescribeCluster':

            if self._nonTerminalRoundTripIndex < 2 and \
               self._nonTerminalRoundTripIndex >= 0:

                self._nonTerminalRoundTripIndex += 1

                return {
                    'cluster': {
                        'name': kwargs[ 'name' ],
                        'status': 'CREATING'
                    }
                }
            elif self._nonTerminalRoundTripIndex == 2:

                self._nonTerminalRoundTripIndex = -1

                return {
                    'cluster': {
                        'name': kwargs[ 'name' ],
                        'status': 'ACTIVE'
                    }
                }
            else:

                self._nonTerminalRoundTripIndex = 0

                return {
                    'cluster': {
                        'name': kwargs[ 'name' ],
                        'status': 'ACTIVE'
                    }
                }
        else:
            raise BaseException( 'Wrong command: %s' % ( operationName ) )


    def test_existingClusterWithDefaultClient( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockNonTerminalResponse
        ):

            result = module.getTerminalEksClusterStatus( 'foo' )

            assert result == 'ACTIVE'


    def test_existingClusterWithCustomClient( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockNonTerminalResponse
        ):

            import boto3

            client = boto3.client( 'eks' )

            result = module.getTerminalEksClusterStatus( 'foo' )

            assert result == 'ACTIVE'


    def test_existingClusterWithCustomWaitPeriod( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockNonTerminalResponse
        ):

            result = module.getTerminalEksClusterStatus( 'foo', None, 1 )

            assert result == 'ACTIVE'



class TestwriteDataToFile():


    def test_singleFileWithDefaults( self, module, maintainKubeManifestDir ):

        data = 'foobar'

        result = module.writeDataToFile( data )

        assert isinstance( result, tuple )

        assert len( result ) == 1

        assert os.path.exists( result[ 0 ] )

        _data = None
        with open( result[ 0 ], 'r' ) as fh:

            _data = fh.read()

        os.remove( result[ 0 ] )

        assert _data == data


    def test_multipleFilesWithDefaults( self, module, maintainKubeManifestDir ):

        data = 'foobar'
        data2 = 'barfoo'

        result = module.writeDataToFile( data, data2 )

        assert isinstance( result, tuple )

        assert len( result ) == 2

        assert os.path.exists( result[ 0 ] )
        
        assert os.path.exists( result[ 1 ] )

        _data = None
        with open( result[ 0 ], 'r' ) as fh:

            _data = fh.read()

        os.remove( result[ 0 ] )

        _data2 = None
        with open( result[ 1 ], 'r' ) as fh:

            _data2 = fh.read()

        os.remove( result[ 1 ] )

        assert _data == data
        assert _data2 == data2


    def test_singleFileWithCustomBaseDirPath( self, module ):

        import tempfile

        baseDirPath = os.path.join( tempfile.gettempdir(), 'kfagj23rlkrf' )

        os.makedirs( baseDirPath, exist_ok = True )

        data = 'foobar'

        result = module.writeDataToFile(
            data,
            baseDirPath = baseDirPath
        )

        assert isinstance( result, tuple )

        assert len( result ) == 1

        assert os.path.exists( result[ 0 ] )

        assert os.path.dirname( result[ 0 ] ) == baseDirPath

        _data = None
        with open( result[ 0 ], 'r' ) as fh:

            _data = fh.read()

        os.remove( result[ 0 ] )

        assert _data == data

        os.rmdir( baseDirPath )


    def test_singleFileWithCustomPrefix( self, module, maintainKubeManifestDir ):

        prefix = 'q34tlkgealk'

        data = 'foobar'

        result = module.writeDataToFile(
            data,
            prefix = prefix
        )

        assert isinstance( result, tuple )

        assert len( result ) == 1

        assert os.path.exists( result[ 0 ] )

        assert os.path.basename( result[ 0 ] ).startswith( prefix ) == True

        _data = None
        with open( result[ 0 ], 'r' ) as fh:

            _data = fh.read()

        os.remove( result[ 0 ] )

        assert _data == data



class Test_getKubeconfig():


    def _mockZeroExitCodeShellResponse( *args, **kwargs ):

        return '', 0


    def _mockNonZeroExitCodeShellResponse( *args, **kwargs ):

        return '', 1


    def test_withDefaults( self, module, mock ):

        with mock.patch(
            '_helper.runShellCommand',
            side_effect=self._mockZeroExitCodeShellResponse
        ):

            result = module.getKubeconfig( 'foo' )

            assert result == module.KUBECONFIGPATH


    def test_withCustomConfigPath( self, module, mock ):

        import tempfile

        configPath = os.path.join( 
            tempfile.gettempdir(),
            'ada42iow',
            'kfa23rlkrf'
        )

        with mock.patch(
            '_helper.runShellCommand',
            side_effect=self._mockZeroExitCodeShellResponse
        ):

            result = module.getKubeconfig( 'foo', configPath = configPath )

        assert result == configPath

        assert os.path.exists( os.path.dirname( configPath ) )

        os.rmdir( os.path.dirname( configPath ) )


    def test_ShellIsFailing( self, module, mock ):

        with mock.patch(
            '_helper.runShellCommand',
            side_effect=self._mockNonZeroExitCodeShellResponse
        ):

            with pytest.raises( module.ShellException ):

                module.getKubeconfig( 'foo' )