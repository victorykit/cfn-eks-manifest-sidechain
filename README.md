# cfn-eks-manifest-sidechain

**AWS CloudFormation Elastic Kubernetes Service Manifest Sidechain** is a plug-in for AWS CloudFormation, to allow the declarative creation of (EKS) Kubernetes Manifests through CloudFormation resource declarations.

## Example

```yml
# ...

# 1. Declare the sidechain resource 
EksManifestSidechain:
	Type: 'AWS::CloudFormation::Stack'
	Properties:
		TemplateURL: '<path-to-cfn-eks-manifest-sidechain-template.yml>
		Parameters:
			RoleArn: <ARN of EKS-mapped IAM role>
			PrimarySubnetId: <String (optional)>
			SecondarySubnetId: <String (optional)>
			TertiarySubnetId: <String (optional)>
			ExecutionTimeout: <Number (optional)>
			Debug: <False | True (optional)>

# 2. Create a custom resource referencing the sidechain
MyKubernetesManifest:
	Type: 'Custom::KubernetesManifest'
	Properties:
		ServiceToken: !GetAtt [ 'EksManifestSidechain', 'Outputs.ServiceToken' ]
		Manifest: |
			apiVersion: v1
			kind: Service
			metadata:
  				name: my-nginx-svc
  				labels:
    				app: nginx
			spec:
  				type: LoadBalancer
  				ports:
  					- port: 80
  				selector:
    				app: nginx

# 2.1 You can also define multiple manifests seperately
MyKubernetesManifests:
	Type: 'Custom::KubernetesManifest'
	Properties:
		ServiceToken: !GetAtt [ 'EksManifestSidechain', 'Outputs.ServiceToken' ]
		Manifest:
			- |
				apiVersion: v1
				kind: Service
				metadata:
  					name: my-nginx-svc
  					labels:
    					app: nginx
				spec:
  					type: LoadBalancer
  					ports:
  						- port: 80
  					selector:
    					app: nginx
    		- |
				apiVersion: v1
				kind: Service
				metadata:
  					name: my-nginx-svc
  					labels:
    					app: nginx
				spec:
  					type: LoadBalancer
  					ports:
  						- port: 80
  					selector:
    					app: nginx

# ...
```

## Setup

The setup will build all required artifacts (CloudFormation Template, Lambda Layer, Lambda Function) and upload them to AWS S3, to be referenced in your CloudFormation stack.

For any type of setup, you are required to have `python` with the `pip`, and `pipenv` modules, as well as a `docker` executable installed.



### Manual Setup

#### 1\. Install build dependencies

```sh
python3 -m pipenv install -d
```

#### 2\. Build artifacts

```sh
python3 -m pipenv run tox
```

#### 3\. Upload artifacts to S3

```sh
python3 -m pipenv run tox -e upload -- s3://bucket-name/example
```

#### 4\. Adapt CloudFormation template

Update every `Code`, and `Content` section of `AWS::Lambda::Function`, and `AWS::Lambda::LayerVersion` resources to reflect the S3 bucket location specified when uploading the artifacts.


### Setup with `taskdog` 

#### 3\. Upload artifacts to S3

```
python3 -m pipenv run tox -e taskdog -- s3://bucket-name/example
```

#### 4\. Deploy

Either use the S3 URL of the CloudFormation template you previously uploaded, or use the template locally stored under `build/{session-id}`